from pydantic import BaseModel
from typing import Optional, List, Union
from queries.pool import pool


class Error(BaseModel):
    message: str


class StepIn(BaseModel):
    name: str
    instruction: str
    recipe_id: int

class StepOut(BaseModel):
    id: int
    name: str
    instruction: str
    recipe_id: int
    recipe_name: Optional[str]


class StepRepository:
    def get_one(self, step_id: int) -> Optional[StepOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT s.id
                             , s.name
                             , s.instruction
                             , s.recipe_id
                             , r.name
                        FROM recipe_steps s
                        LEFT JOIN recipes r ON (r.id = s.recipe_id)
                        WHERE s.id = %s
                        """,
                        [step_id],
                    )
                    record = result.fetchone()
                    print("RECORD!!!!!!!!!!!!!!!!!!!!!!!!", record)
                    if record is None:
                        return None
                    return self.record_to_step_out(record)
        except Exception as e:
            print("EXCEPTION!!!!!!!!!!!!!!!!!!!!", e)
            return {"message": "Could not get that step"}

    def delete(self, step_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM recipe_steps
                        WHERE id = %s
                        """,
                        [step_id],
                    )
                    return True
        except Exception:
            return False

    def update(
        self, step_id: int, step: StepIn
    ) -> Union[StepOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE recipe_steps
                        SET name = %s
                          , instruction = %s
                          , recipe_id = %s
                        WHERE id = %s
                        """,
                        [
                            step.name,
                            step.instruction,
                            step.recipe_id,
                            step_id,
                        ],
                    )
                    return self.step_in_to_out(step_id, step)
        except Exception:
            return {"message": "Could not update that step"}

    def get_all(self) -> Union[List[StepOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT s.id
                             , s.name
                             , s.instruction
                             , s.recipe_id
                             , r.name
                        FROM recipe_steps s
                        LEFT JOIN recipes r ON (s.recipe_id = r.id)
                        """
                    )
                    return [
                        self.record_to_step_out(record) for record in result
                    ]
        except Exception:
            return {"message": "Could not get all steps"}

    def create(self, step: StepIn) -> Union[StepOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO recipe_steps
                            (name
                            , instruction
                            , recipe_id)
                        VALUES
                            (%s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            step.name,
                            step.instruction,
                            step.recipe_id,
                        ],
                    )
                    id = result.fetchone()[0]
                    return self.step_in_to_out(id, step)
        except Exception:
            return {"message": "Create did not work"}

    def step_in_to_out(self, id: int, step: StepIn):
        old_data = step.dict()
        return StepOut(id=id, **old_data)

    def record_to_step_out(self, record):
        return StepOut(
            id=record[0],
            name=record[1],
            instruction=record[2],
            recipe_id=record[3],
            recipe_name=record[4]
        )
