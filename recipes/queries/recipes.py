from pydantic import BaseModel
from typing import Optional, List, Union
from queries.pool import pool


class Error(BaseModel):
    message: str


class RecipeIn(BaseModel):
    name: str
    description: str
    difficulty: int
    picture_url: str

class RecipeOut(BaseModel):
    id: int
    name: str
    description: str
    difficulty: int
    picture_url: str


class RecipeRepository:
    def get_one(self, recipe_id: int) -> Optional[RecipeOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                             , name
                             , description
                             , difficulty
                             , picture_url
                        FROM recipes
                        WHERE id = %s
                        """,
                        [recipe_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_recipe_out(record)
        except Exception:
            return {"message": "Could not get that recipe"}

    def delete(self, recipe_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM recipes
                        WHERE id = %s
                        """,
                        [recipe_id],
                    )
                    return True
        except Exception:
            return False

    def update(
        self, recipe_id: int, recipe: RecipeIn
    ) -> Union[RecipeOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE recipes
                        SET name = %s
                          , description = %s
                          , difficulty = %s
                          , picture_url = %s
                        WHERE id = %s
                        """,
                        [
                            recipe.name,
                            recipe.description,
                            recipe.difficulty,
                            recipe.picture_url,
                            recipe_id,
                        ],
                    )
                    return self.recipe_in_to_out(recipe_id, recipe)
        except Exception:
            return {"message": "Could not update that recipe"}

    def get_all(self) -> Union[List[RecipeOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                             , name
                             , description
                             , difficulty
                             , picture_url
                        FROM recipes
                        """
                    )
                    return [
                        self.record_to_recipe_out(record) for record in result
                    ]
        except Exception:
            return {"message": "Could not get all recipes"}

    def create(self, recipe: RecipeIn) -> Union[RecipeOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO recipes
                            (name
                            , description
                            , difficulty
                            , picture_url)
                        VALUES
                            (%s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            recipe.name,
                            recipe.description,
                            recipe.difficulty,
                            recipe.picture_url,
                        ],
                    )
                    id = result.fetchone()[0]
                    return self.recipe_in_to_out(id, recipe)
        except Exception:
            return {"message": "Create did not work"}

    def recipe_in_to_out(self, id: int, recipe: RecipeIn):
        old_data = recipe.dict()
        return RecipeOut(id=id, **old_data)

    def record_to_recipe_out(self, record):
        return RecipeOut(
            id=record[0],
            name=record[1],
            description=record[2],
            difficulty=record[3],
            picture_url=record[4]
        )
