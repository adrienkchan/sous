from fastapi import APIRouter, Depends, Response
from typing import Union, List, Optional
from queries.steps import (
    StepIn,
    StepRepository,
    StepOut,
    Error,
)

router = APIRouter()

@router.post("/steps", response_model=Union[StepOut, Error])
def create_step(
    step: StepIn,
    repo: StepRepository = Depends(),
):
    try:
        return repo.create(step)
    except Exception as e:
        return Error(message=str(e))


@router.get("/steps", response_model=Union[List[StepOut], Error])
def get_all(
    repo: StepRepository = Depends(),
):
    return repo.get_all()


@router.put(
    "/steps/{step_id}", response_model=Union[StepOut, Error]
)
def update_step(
    step_id: int,
    step: StepIn,
    repo: StepRepository = Depends(),
) -> Union[Error, StepOut]:
    return repo.update(step_id, step)


@router.delete("/steps/{step_id}", response_model=bool)
def delete_step(
    step_id: int,
    repo: StepRepository = Depends(),
) -> bool:
    return repo.delete(step_id)


@router.get(
    "/steps/{step_id}", response_model=Optional[StepOut]
)
def get_one_step(
    step_id: int,
    response: Response,
    repo: StepRepository = Depends(),
) -> StepOut:
    step = repo.get_one(step_id)
    print("STEP!!!!!!!!", step)
    if step is None:
        response.status_code = 404
    return step
