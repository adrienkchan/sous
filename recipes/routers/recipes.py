from fastapi import APIRouter, Depends, Response
from typing import Union, List, Optional
from queries.recipes import (
    RecipeIn,
    RecipeRepository,
    RecipeOut,
    Error,
)

router = APIRouter()

@router.post("/recipes", response_model=Union[RecipeOut, Error])
def create_recipe(
    recipe: RecipeIn,
    repo: RecipeRepository = Depends(),
):
    try:
        return repo.create(recipe)
    except Exception as e:
        return Error(message=str(e))


@router.get("/recipes", response_model=Union[List[RecipeOut], Error])
def get_all(
    repo: RecipeRepository = Depends(),
):
    return repo.get_all()


@router.put(
    "/recipes/{recipe_id}", response_model=Union[RecipeOut, Error]
)
def update_recipe(
    recipe_id: int,
    recipe: RecipeIn,
    repo: RecipeRepository = Depends(),
) -> Union[Error, RecipeOut]:
    return repo.update(recipe_id, recipe)


@router.delete("/recipes/{recipe_id}", response_model=bool)
def delete_recipe(
    recipe_id: int,
    repo: RecipeRepository = Depends(),
) -> bool:
    return repo.delete(recipe_id)


@router.get(
    "/recipes/{recipe_id}", response_model=Optional[RecipeOut]
)
def get_one_recipe(
    recipe_id: int,
    response: Response,
    repo: RecipeRepository = Depends(),
) -> RecipeOut:
    recipe = repo.get_one(recipe_id)
    if recipe is None:
        response.status_code = 404
    return recipe
