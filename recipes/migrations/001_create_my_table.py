steps = [
    [
        """
        CREATE TABLE recipes (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(1000) NOT NULL,
            description VARCHAR(1000) NOT NULL,
            difficulty SMALLINT NOT NULL CHECK (difficulty < 11),
            picture_url VARCHAR(1000) NOT NULL
        );
        """,
        """
        DROP TABLE recipes;
        """,
    ],
    [
        """
        CREATE TABLE recipe_steps (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(1000) NOT NULL,
            instruction VARCHAR(10000) NOT NULL,
            recipe_id INT NOT NULL REFERENCES recipes(id)
        );
        """,
        """
        DROP TABLE recipe_steps;
        """,
    ],
    [
        """
        CREATE TABLE recipe_ingredients (
            id SERIAL PRIMARY KEY NOT NULL,
            ingredient VARCHAR(1000) NOT NULL,
            amount SMALLINT NOT NULL CHECK (amount > 0),
            recipe_id INT NOT NULL REFERENCES recipes(id)
        );
        """,
        """
        DROP TABLE recipe_ingredients;
        """,
    ],
]
